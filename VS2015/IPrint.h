#pragma once
class IPrint {
public:
	virtual auto print()const->void = 0;
	virtual ~IPrint() = default;
};