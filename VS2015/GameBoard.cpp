#include "GameBoard.h"


GameBoard::GameBoard()
{
	for (auto i = 0; i < FIRST_CARDS; i++)
	{
		cardsOnBoard.push_back(Johnny.dealCard());
	}
}

auto GameBoard::reset()->void
{
	gameState = GameState::On;
	// TODO: pupulate again
}

auto GameBoard::startGame()->void
{
	while (gameIsOn(SetsAreAvailable::Yes) == GameState::On) {
		// ?? how do we know that sets are still available
		// this looks like another state of the game that needs to be added as a member
		F1();
	}
}

auto GameBoard::F1()->bool
{
	auto setNotFound = true;
	size_t index1 = 0;
	size_t index2 = index1 + 1;
	size_t index3 = index2 + 1;
	size_t size = cardsOnBoard.size();
	for (; index1 < size - 2; index1++)
	{
		for (; index2 < size - 1 && setNotFound; index2++)
		{
			for (; index3 < size && setNotFound; index3++)
			{
				if (F2(index1, index2, index3) == SetValidation::Valid)
				{
					setNotFound = false;
				}
			}
		}
	}
	if (setNotFound == false)
	{
		removeSet(index1 - 1, index2 - 1, index3 - 1);
		//add 3 new cards on game board
		for (int i = 0; i < CARDS_PER_DEAL; i++)
			cardsOnBoard.push_back(getNewCardFromDealer());
	}
	if (index1 == size - 2 && index2 == size - 1 && index3 == size)
	{
		gameIsOn(SetsAreAvailable::No);
	}

	return !setNotFound;
}


auto GameBoard::F2(const CardIndexInDeck index1, const CardIndexInDeck index2, const CardIndexInDeck index3)->SetValidation
{
	//aka validation
	Card c1 = cardsOnBoard[index1];
	Card c2 = cardsOnBoard[index2];
	Card c3 = cardsOnBoard[index3];

	static const int  SUM_FOR_VALIDATION = 3;

	int testingCase1 = c1.checkNumber(c2);
	int testingCase2 = c1.checkNumber(c3);
	int testingCase3 = c2.checkNumber(c3);
	if (testingCase1 || testingCase2 || testingCase3)
	{
		if (testingCase1 + testingCase2 + testingCase3 != SUM_FOR_VALIDATION)
			return SetValidation::Unvalid;
	}

	testingCase1 = c1.checkSymbol(c2);
	testingCase2 = c1.checkSymbol(c3);
	testingCase3 = c2.checkSymbol(c3);

	if (testingCase1 || testingCase2 || testingCase3)
	{
		if (testingCase1 + testingCase2 + testingCase3 != SUM_FOR_VALIDATION)
			return SetValidation::Unvalid;
	}

	testingCase1 = c1.checkShading(c2);
	testingCase2 = c1.checkShading(c3);
	testingCase3 = c2.checkShading(c3);

	if (testingCase1 || testingCase2 || testingCase3)
	{
		if (testingCase1 + testingCase2 + testingCase3 != SUM_FOR_VALIDATION)
			return SetValidation::Unvalid;
	}


	testingCase1 = c1.checkColor(c2);
	testingCase2 = c1.checkColor(c3);
	testingCase3 = c2.checkColor(c3);

	if (testingCase1 || testingCase2 || testingCase3)
	{
		if (testingCase1 + testingCase2 + testingCase3 != SUM_FOR_VALIDATION)
			return SetValidation::Unvalid;
	}
	return SetValidation::Valid;
}

auto GameBoard::removeSet(CardIndexInDeck index1, CardIndexInDeck index2, CardIndexInDeck index3)->RemoveState
{
	orderIndexes(index1, index2, index3);
	cardsOnBoard.erase(cardsOnBoard.begin() + index3);
	cardsOnBoard.erase(cardsOnBoard.begin() + index2);
	cardsOnBoard.erase(cardsOnBoard.begin() + index1);
	return RemoveState::Success;
}

auto GameBoard::getNewCardFromDealer()->const Card
{
	return Johnny.dealCard();
}

auto GameBoard::orderIndexes(CardIndexInDeck & index1, CardIndexInDeck & index2, CardIndexInDeck & index3)->void
{
	if (index1 > index2)
	{
		index1 ^= index2;
		index2 ^= index1;
		index1 ^= index2;
	}
	if (index1 > index3)
	{
		index1 ^= index3;
		index3 ^= index1;
		index1 ^= index3;
	}
	if (index2 > index3)
	{
		index2 ^= index3;
		index3 ^= index2;
		index2 ^= index3;
	}

}
auto GameBoard::gameIsOn(SetsAreAvailable setsAreStillAvailable)->GameState
{

	if (cardsOnBoard.size() < MAX_NUMBER_OF_CARDS_ON_BOARD || setsAreStillAvailable == SetsAreAvailable::No)
	{
		gameState = GameState::Terminated;
	}

	return gameState;
}

