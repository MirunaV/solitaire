#pragma once
#include <memory>
#include <vector>
#include "IStart.h"
#include "Card.h"
#include "Dealer.h"

enum class SetsAreAvailable : bool {
	No,
	Yes
};

enum class GameState : bool {
	On,
	Terminated
};
enum class RemoveState : bool {
	Fail,
	Success
};

enum class SetValidation : bool {
	Unvalid,
	Valid
};


class GameBoard : public IStart
{
	typedef int CardIndexInDeck;
	static const int FIRST_CARDS = 12;
	static const int MAX_NUMBER_OF_CARDS_ON_BOARD = 12;
	static const int CARDS_PER_DEAL = 3;
public:
	GameBoard();
	~GameBoard() = default;
	auto startGame()->void override;
	auto F1()->bool;
	auto F2(const CardIndexInDeck index1, const CardIndexInDeck index2, const CardIndexInDeck index3)->SetValidation;
	auto removeSet(CardIndexInDeck index1, CardIndexInDeck index2, CardIndexInDeck index3)->RemoveState;
	auto getNewCardFromDealer()->const Card;
	auto reset()->void;
private:
	auto orderIndexes(CardIndexInDeck &index1, CardIndexInDeck& index2, CardIndexInDeck& index3)->void;
	auto gameIsOn(SetsAreAvailable setsAreStillAvailable)->GameState;
private:
	Dealer Johnny;
	std::vector<Card> cardsOnBoard;
	GameState gameState;
};

