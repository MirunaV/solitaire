#include "Card.h"



Card::Card(NUMBER number, SYMBOL symbol, SHADING shading, COLOR color)
	:number(number)
	, symbol(symbol)
	, shading(shading)
	, color(color)
{
}

auto Card::print()const->void
{
	static std::map<const NUMBER, const char*> numberString =
	{
		{NUMBER::ONE,"ONE"},
		{NUMBER::TWO,"TWO"},
		{NUMBER::THREE,"THREE"}
	};

	static std::map<const SYMBOL, const char*> symbolString =
	{
		{	SYMBOL::DIAMOND,"DIAMOND" },
		{ SYMBOL::OVAL,"OVAL" },
		{ SYMBOL::SQIGGLE,"SQIGGLE" }
	};
	static std::map<const SHADING, const char*> shadingString =
	{
		{SHADING::OPEN,"OPEN" },
		{SHADING::SOLID,"SOLID" },
		{ SHADING::STRIPED,"STRIPED" }
	};
	static std::map<const COLOR, const char*> colorString =
	{
		{COLOR::BLUE,"BLUE" },
		{COLOR::GREEN,"GREEN" },
		{ COLOR::RED,"RED" }
	};

	std::cout << "(" <<
		numberString[number] << "," << symbolString[symbol]
		<< "," << shadingString[shading] << "," << colorString[color]
		<< ")" << std::endl;
}

auto Card::getNumber()const noexcept -> NUMBER
{
	return number;
}

auto Card::getSymbol()const noexcept-> SYMBOL
{
	return symbol;
}

auto Card::getShading()const noexcept-> SHADING
{
	return shading;
}

auto Card::getColor()const noexcept-> COLOR
{
	return color;
}

auto Card::checkNumber(const Card & that) const ->  bool
{
	return number == that.number;
}

auto Card::checkSymbol(const Card & that) const ->  bool
{
	return symbol == that.symbol;
}

auto Card::checkShading(const Card & that) const ->  bool
{
	return shading == that.shading;
}

auto Card::checkColor(const Card & that) const ->  bool
{
	return color == that.color;
}


