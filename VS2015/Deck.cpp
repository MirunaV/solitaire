#include "Deck.h"



Deck::Deck()
{
	cards = std::vector<Card>();
	auto sizeOfNumber = static_cast<int>(NUMBER::size);
	auto sizeOfSymbol = static_cast<int>(SYMBOL::size);
	auto sizeOfShading = static_cast<int>(SHADING::size);
	auto sizeOfColor = static_cast<int>(COLOR::size);
	for (auto numberIndex = static_cast<int>(NUMBER::ONE); numberIndex < sizeOfNumber; numberIndex++)
	{
		for (int symbolIndex = static_cast<int>(SYMBOL::DIAMOND); symbolIndex < sizeOfSymbol; symbolIndex++)
		{
			for (int shadeIndex = static_cast<int>(SHADING::SOLID); shadeIndex < sizeOfShading; shadeIndex++)
			{
				for (int colorIndex = static_cast<int>(COLOR::RED); colorIndex < sizeOfColor; colorIndex++)
				{
					cards.push_back(Card(static_cast<NUMBER>(numberIndex), static_cast<SYMBOL>(symbolIndex),
						static_cast<SHADING>(shadeIndex), static_cast<COLOR>(colorIndex)));
				}
			}

		}

	}
	std::random_shuffle(cards.begin(), cards.end());
}



auto Deck::print()const->void
{
	for (auto card : cards)
	{
		card.print();
	}
}

auto Deck::giveLastCard()->const Card
{
	const auto lastCard = cards.back();
	cards.pop_back();
	return lastCard;
}
