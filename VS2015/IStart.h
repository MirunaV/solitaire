#pragma once
class IStart {
public:
	virtual auto startGame()->void = 0;
	virtual ~IStart() = default;
};