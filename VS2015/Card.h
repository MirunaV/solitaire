#pragma once
#include <iostream>
#include <map>
#include "IPrint.h"

enum class NUMBER {
	ONE,
	TWO,
	THREE,
	size
};

enum class SYMBOL {
	DIAMOND,
	SQIGGLE,
	OVAL,
	size
};
enum class SHADING {
	SOLID,
	STRIPED,
	OPEN,
	size
};
enum class COLOR {
	RED,
	GREEN,
	BLUE,
	size
};

class Card :public IPrint
{
public:
	Card() = delete; // not generated if other ctor is defined
	Card(NUMBER number = NUMBER::ONE, SYMBOL symbol = SYMBOL::DIAMOND, SHADING shading = SHADING::OPEN, COLOR color = COLOR::BLUE);
	~Card() = default;
	auto print()const->void override;
	auto getNumber()const noexcept ->NUMBER;
	auto getSymbol() const noexcept->SYMBOL;
	auto getShading()const noexcept->SHADING;
	auto getColor()const noexcept-> COLOR;
	auto checkNumber(const Card& that)const->bool;
	auto checkSymbol(const Card& that)const->bool;
	auto checkShading(const Card& that)const-> bool;
	auto checkColor(const Card& that)const ->bool;
private:
	// typeedef std::tuple< NUMBER, SYMBOL, SHADING, COLOR> CardInfo // std::get< 0 >
	NUMBER number;
	SYMBOL symbol;
	SHADING shading;
	COLOR color;
};

