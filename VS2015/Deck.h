#pragma once
#include <vector>
#include <algorithm>
#include "Card.h"
class Deck : public IPrint
{
public:
	Deck();
	~Deck() = default;
	auto print()const->void override;
	auto giveLastCard()->const Card;
private:
	std::vector<Card> cards;
};

