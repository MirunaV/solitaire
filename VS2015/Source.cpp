#include "GameBoard.h"


int main() {

	std::unique_ptr<IStart> game = std::make_unique<GameBoard>();
	//auto game = std::unique_ptr<IStart>(new GameBoard()); new leaks 
	game->startGame();
	//game->reset();
	//game->startGame();

	return 0;
}


/*
GameBoardManager
o lista de gameBoards

GetGameBoardByIndex(index);
GetGameBoardByName(name);
GetGameBoardsCount();
CreateGameBoard();
RemoveGameBoardByIndex(index);
RemoveGameBoardByName(name);

*/